# -*- coding: utf-8 -*-
# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.

{
    'name': 'Business Snippet',
    'author': 'Flectra',
    'version': '1.0',
    'category': 'Website',
    
    'website': 'https://flectrahq.com/',
    'depends': [],
    'data': [
 
    ],
    'installable': True,
    #'price': 200,
    'license': 'Other proprietary',
    #'currency': 'EUR',

}
